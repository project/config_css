This module provides a simple config entity to put custom CSS on pages using the same visibility condition system as blocks.

The stylesheets will be placed at the very last position on the page, allowing content editors to create additional CSS for sub-sections of the site.