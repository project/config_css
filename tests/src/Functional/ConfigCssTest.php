<?php

namespace Drupal\Tests\config_css\Functional;

use Drupal\config_css\Entity\ConfigCss;
use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;

/**
 * Config CSS browser tests.
 * 
 * @group config_css 
 */
class ConfigCssTest extends BrowserTestBase {

  protected $defaultTheme = 'stable';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['node', 'config_css', 'block'];

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->placeBlock('local_tasks_block');
    $this->placeBlock('local_actions_block');
    $this->placeBlock('page_title_block');
    $this->placeBlock('system_breadcrumb_block');

    $permissions = [
      'administer config_css',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
  }

  /**
   * Enable CSS aggregation.
   */
  protected function enableCssAggregation() {
    $config = $this->config('system.performance');
    $config->set('css.preprocess', 1);
    $config->save();
    drupal_flush_all_caches();
  }

  /**
   * Test creating/editing config via the user interface.
   */
  public function testConfigAdminUI() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/structure/config-css');
    $this->assertElementPresent('a[href="/admin/structure/config-css/add"]');
    $this->assertContains('There are no stylesheets yet.', $this->cssSelect('main table')[0]->getText());
    $this->click('a[href="/admin/structure/config-css/add"]');

    // Create test stylesheet, visible on all pages.
    $this->drupalPostForm(NULL, [
      'label' => 'Test stylesheet 1',
      'id' => 'test_stylesheet_1',
      'stylesheet' => 'body.configcsstestclass {color: blue;}',
      'theme' => 'stable',
      'weight' => 5,
    ], 'Save');

    // Check listing page for new stylesheet.
    $this->drupalGet('admin/structure/config-css');
    $row = $this->cssSelect('main table tr:first-child td');
    $this->assertEquals('Test stylesheet 1', $row[0]->getText());
    $this->assertEquals('test_stylesheet_1', $row[1]->getText());
    $this->assertEquals('Stable', $row[2]->getText());
    $this->assertEquals('5', $row[3]->getText());

    // Check stylesheet is visible on front page.
    $this->drupalGet('<front>');
    $stylesheets = $this->cssSelect('link[rel="stylesheet"]');
    // The stylesheet should be last.
    $this->assertContains('files/config_css/test_stylesheet_1.css', end($stylesheets)->getAttribute('href'));

    // Turn on aggregation.
    $this->enableCssAggregation();

    // Check stylesheet is visible on front page.
    $this->drupalGet('<front>');
    $stylesheets = $this->cssSelect('link[rel="stylesheet"]');
    // The stylesheet should be last.
    $path = end($stylesheets)->getAttribute('href');
    $this->drupalGet($path);
    $css = $this->getSession()->getPage()->getContent();
    $this->assertEquals('body.configcsstestclass{color:blue;}', $css);

    // Edit the config.
    $this->drupalGet('admin/structure/config-css/test_stylesheet_1');
    $this->drupalPostForm(NULL, [
      'stylesheet' => 'body.configcsstestclass {color: purple;}',
    ], 'Save');

    // Check updated stylesheet is visible on front page.
    $this->drupalGet('<front>');
    $stylesheets = $this->cssSelect('link[rel="stylesheet"]');
    // The stylesheet should be last.
    $path = end($stylesheets)->getAttribute('href');
    $this->drupalGet($path);
    $css = $this->getSession()->getPage()->getContent();
    $this->assertEquals('body.configcsstestclass{color:purple;}', $css);
  }

  /**
   * Test stylesheet weighting.
   */
  public function testWeight() {
    ConfigCss::create([
      'id' => 'test_stylesheet_weight_1',
      'label' => 'Test stylesheet weight 1',
      'stylesheet' => 'body.configcsstestclass {color: blue;}',
      'theme' => 'stable',
      'weight' => 1,
    ])->save();

    ConfigCss::create([
      'id' => 'test_stylesheet_weight_2',
      'label' => 'Test stylesheet weight 2',
      'stylesheet' => 'body.configcsstestclass {color: purple;}',
      'theme' => 'stable',
      'weight' => 2,
    ])->save();

    // Check stylesheets are visible on front page.
    $this->drupalGet('<front>');
    $stylesheets = $this->cssSelect('link[rel="stylesheet"]');
    // The two stylesheets should be last and second last.
    $count = count($stylesheets);
    $this->assertContains('files/config_css/test_stylesheet_weight_1.css', $stylesheets[$count - 2]->getAttribute('href'));
    $this->assertContains('files/config_css/test_stylesheet_weight_2.css', $stylesheets[$count - 1]->getAttribute('href'));

    // Turn on aggregation.
    $this->enableCssAggregation();

    // Both stylesheets should have been combined in the correct order.
    $this->drupalGet('<front>');
    $stylesheets = $this->cssSelect('link[rel="stylesheet"]');
    // The stylesheet should be last.
    $path = end($stylesheets)->getAttribute('href');
    $this->drupalGet($path);
    $css = $this->getSession()->getPage()->getContent();
    $this->assertEquals("body.configcsstestclass{color:blue;}\nbody.configcsstestclass{color:purple;}", $css);

    // Update the weight and verify the result.
    $css = ConfigCss::load('test_stylesheet_weight_1');
    $css->setWeight(3);
    $css->save();
    // Both stylesheets should have been combined in the correct order.
    $this->drupalGet('<front>');
    $stylesheets = $this->cssSelect('link[rel="stylesheet"]');
    // The stylesheet should be last.
    $path = end($stylesheets)->getAttribute('href');
    $this->drupalGet($path);
    $css = $this->getSession()->getPage()->getContent();
    $this->assertEquals("body.configcsstestclass{color:purple;}\nbody.configcsstestclass{color:blue;}", $css);
  }

  /**
   * Test visibility rules
   */
  public function testVisibility() {
    $testContentType = $this->drupalCreateContentType(['type' => 'config_css_test', 'name' => 'Config CSS test']);

    $node = Node::create([
      'type' => $testContentType->id(),
      'title' => 'Visibility test node',
      'body' => '<p>Visibility test node</p>',
    ]);
    $node->save();

    ConfigCss::create([
      'id' => 'test_stylesheet_all_pages',
      'label' => 'Test stylesheet all pages',
      'stylesheet' => 'body.configcsstestclass {color: blue;}',
      'theme' => 'stable',
      'weight' => 0,
    ])->save();

    ConfigCss::create([
      'id' => 'test_stylesheet_admin_pages',
      'label' => 'Test stylesheet admin pages',
      'stylesheet' => 'body.configcsstestclass {color: purple;}',
      'theme' => 'stable',
      'weight' => 1,
      'visibility' => [
        'request_path' => [
          'id' => 'request_path',
          'pages' => '/node/*',
          'negate' => FALSE,
          'context_mapping' => [],
        ],
      ],
    ])->save();

    // Check only first stylesheet shows on home page.
    $this->drupalGet('<front>');
    $stylesheets = $this->cssSelect('link[rel="stylesheet"]');
    foreach ($stylesheets as $stylesheet) {
      $href = $stylesheet->getAttribute('href');
      if (strpos($href, 'config_css') !== FALSE) {
        $this->assertContains('files/config_css/test_stylesheet_all_pages.css', $href);
      }
    }

    // Check first and second stylesheets show on admin page
    $this->drupalGet("node/{$node->id()}");
    $stylesheets = $this->cssSelect('link[rel="stylesheet"]');
    // The two stylesheets should be last and second last.
    $count = count($stylesheets);
    $this->assertContains('files/config_css/test_stylesheet_all_pages.css', $stylesheets[$count - 2]->getAttribute('href'));
    $this->assertContains('files/config_css/test_stylesheet_admin_pages.css', $stylesheets[$count - 1]->getAttribute('href'));
  }
}
