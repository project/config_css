<?php

namespace Drupal\config_css\Entity;

use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\config_css\ConfigCssInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Url;

/**
 * Defines the config_css entity type.
 *
 * @ConfigEntityType(
 *   id = "config_css",
 *   label = @Translation("Stylesheet"),
 *   label_collection = @Translation("Stylesheets"),
 *   label_singular = @Translation("stylesheet"),
 *   label_plural = @Translation("stylesheets"),
 *   label_count = @PluralTranslation(
 *     singular = "@count stylesheet",
 *     plural = "@count stylesheets",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\config_css\ConfigCssListBuilder",
 *     "form" = {
 *       "add" = "Drupal\config_css\Form\ConfigCssForm",
 *       "edit" = "Drupal\config_css\Form\ConfigCssForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "config_css",
 *   admin_permission = "administer config_css",
 *   links = {
 *     "collection" = "/admin/structure/config-css",
 *     "add-form" = "/admin/structure/config-css/add",
 *     "edit-form" = "/admin/structure/config-css/{config_css}",
 *     "delete-form" = "/admin/structure/config-css/{config_css}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "theme",
 *     "weight",
 *     "visibility",
 *     "stylesheet"
 *   }
 * )
 */
class ConfigCss extends ConfigEntityBase implements EntityWithPluginCollectionInterface {

  /**
   * The ID of the configuration CSS.
   *
   * @var string
   */
  protected $id;

  /**
   * The label of the configuration CSS.
   *
   * @var string
   */
  protected $label;

  /**
   * The weight of the CSS.
   *
   * @var int
   */
  protected $weight;

  /**
   * The theme that this CSS applies to.
   *
   * @var string
   */
  protected $theme;

  /**
   * The visibility settings for this CSS.
   *
   * @var array
   */
  protected $visibility = [];

  /**
   * The stylesheet contents.
   *
   * @var string
   */
  protected $stylesheet = '';

  /**
   * The visibility collection.
   *
   * @var \Drupal\Core\Condition\ConditionPluginCollection
   */
  protected $visibilityCollection;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Executable\ExecutableManagerInterface
   */
  protected $conditionPluginManager;



  /**
   * {@inheritdoc}
   */
  public function getTheme() {
    return $this->theme;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * Sorts CSS by weight
   */
  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    // Sort by weight.
    $weight = $a->getWeight() - $b->getWeight();
    if ($weight) {
      return $weight;
    }

    // Sort by label.
    return strcmp($a->label(), $b->label());
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    $this->addDependency('theme', $this->theme);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisibility() {
    return $this->getVisibilityConditions()->getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getVisibilityConditions() {
    if (!isset($this->visibilityCollection)) {
      $visibility = $this->get('visibility');
      foreach ($visibility as $key => $plugin) {
        $visibility[$key]['id'] = $key;
      }
      $this->visibilityCollection = new ConditionPluginCollection($this->conditionPluginManager(), $visibility);
    }
    return $this->visibilityCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisibilityCondition($instance_id) {
    return $this->getVisibilityConditions()->get($instance_id);
  }

  /**
   * Gets the condition plugin manager.
   *
   * @return \Drupal\Core\Executable\ExecutableManagerInterface
   *   The condition plugin manager.
   */
  protected function conditionPluginManager() {
    if (!isset($this->conditionPluginManager)) {
      $this->conditionPluginManager = \Drupal::service('plugin.manager.condition');
    }
    return $this->conditionPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'visibility' => $this->getVisibilityConditions(),
    ];
  }

  /**
   * Get the URL to the stored stylesheet file.
   *
   * @return string
   */
  public function getStylesheetUrl() {
    $config = \Drupal::config('config_css.settings');
    $path = rtrim($config->get('css_store_path'), '/') . '/' . $this->id() . '.css';
    return file_url_transform_relative(\Drupal::service('stream_wrapper_manager')->getViaUri($path)->getExternalUrl());
  }
}
