<?php

namespace Drupal\config_css;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ThemeHandler;

/**
 * Provides a listing of config_csses.
 */
class ConfigCssListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['theme'] = $this->t('Theme');
    $header['weight'] = $this->t('Weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\config_css\ConfigCssInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['theme'] = \Drupal::service('theme_handler')->getTheme($entity->get('theme'))->info['name'];
    $row['weight'] = $entity->get('weight');
    return $row + parent::buildRow($entity);
  }

}
