<?php

namespace Drupal\config_css\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Configuration CSS settings for this site.
 */
class ConfigCssSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_css_config_css_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config_css.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['css_store_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS store path'),
      '#default_value' => $this->config('config_css.settings')->get('css_store_path'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('config_css.settings')
      ->set('css_store_path', $form_state->getValue('css_store_path'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
