<?php

namespace Drupal\config_css;

/**
 * This class exists just so we can access the resolveConditions method in ConditionAccessResolverTrait
 */
class ConditionAccessResolver {
  use \Drupal\Core\Condition\ConditionAccessResolverTrait {
    resolveConditions as public;
  }
}