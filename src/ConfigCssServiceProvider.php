<?php

namespace Drupal\config_css;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Defines a service provider for the Configuration CSS module.
 */
class ConfigCssServiceProvider extends ServiceProviderBase {
  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($definition = $container->getDefinition('asset.resolver')) {
      // Use a separate cache bin for the asset resolver so we can clear it.
      $definition->setArgument(5, new Reference('cache.css_config_asset_resolver'));
    }
  }
}
